import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { EmeiService } from '../_services/check-emei.service';
import { EmeiService } from '../_services';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  text = 'Default starting text';
  form: FormGroup;
  resData: any;
  imgDefault = '';
  txtEmei;
  resSucces = 9;
  constructor(
    private fb: FormBuilder,
    private emeiService: EmeiService,
  ) {
    this.form = this.fb.group({
      emei: ['', [Validators.required]],
    });
    this.imgDefault =  environment.url + '/theme/self/imgs/logo-cocoshine-1.png';
  }

  get f() {
    return this.form.controls;
  }

  checkEMEI() {
    console.log(this.form.value.emei);
    this.txtEmei = this.form.value.emei;
    console.log(this.txtEmei);
    this.emeiService.findproductByEmei(this.form.value.emei).subscribe(
      (res: any) => {
        console.log(res);
        this.resSucces = res.status;
        if (res.status) {
          this.resData = res;
        }
      },
      error => {

      });
  }
}
