import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

export class BaseService {
    http: HttpClient;
    apiUrl: string;

    constructor(http, apiUrl) {
      this.http = http;
      this.apiUrl = apiUrl;
    }
}