import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmeiService extends BaseService {

  constructor(http: HttpClient) {
    super(http, `${environment.apiUrl}/get-product-by-imei`);
  }

  findproductByEmei(emei: string) {
    return this.http.get(`${this.apiUrl}?imei=${emei}`);
  }
}
